﻿using System;
using System.Fakes;
using System.Net;
using System.Net.Fakes;
using System.Collections.Generic;
using Tregg;
using Tregg.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.QualityTools.Testing.Fakes;
using System.Threading.Tasks;

namespace TestTreggAPI
{
    [TestClass]
    public class TestTreggUser
    {
        string responseUsername, responseAccessToken = "";

        void callbackSignUp(TreggUser user, TreggException e)
        {
            string expectedName = "foo";
            string expectedPass = "foobar";

            Assert.AreEqual(responseUsername,expectedName);
            Assert.AreEqual(user.Password,expectedPass);
        }

        void callbackSignIn(TreggUser user, TreggException e)
        {
            string expectedAccessToken = "iqcpdpxexqbstiw766l8fz29l9k3fsd";

            Assert.AreEqual(responseAccessToken, expectedAccessToken);
        }

        void callbackSignInWithFB(TreggUser user, TreggException e)
        {
            string expectedAccessToken = "iqcpdpxexqbstiw766l8fz29l9k3fsd";

            Assert.AreEqual(responseAccessToken, expectedAccessToken);
        }

        [TestMethod]
        public void TestSignUp()
        {
            using (ShimsContext.Create())
            {
                ShimTreggAPI.CreateHttpRequestStringStringDictionaryOfStringStringTreggAPITreggApiDelegateBooleanString = (string url, string method, Dictionary<string, string> data, TreggAPI.TreggApiDelegate callback, bool isAuthenticated, string contentType) =>
                    {
                        string responseString = "{\"user\":{\"id\":10,\"created_at\":\"2013-10-04T13:43:31.055Z\",\"updated_at\":\"2013-10-04T13:43:31.055Z\",\"username\":\"foo\",\"password\":\"$2a$10$xJKoqUeA7IAwebdNHNkeie/x.gmjvQZEO83cTy.44pSyBpVF9lxWq\",\"reset_pwd_token\":null,\"password_reset_sent_at\":null,\"facebook_uid\":null,\"facebook_first_name\":null,\"facebook_last_name\":null,\"facebook_username\":null,\"facebook_picture\":null,\"facebook_gender\":null,\"facebook_birthday\":null,\"role\":null},\"access_token\":\"iqcpdpxexqbstiw766l8fz29l9k3fsd\"}";
                        var result = MiniJSON.Json.Deserialize(responseString) as Dictionary<string, object>;
                        Dictionary<string, object> userData = result["user"] as Dictionary<string, object>;
                        responseUsername = (string)userData["username"];

                        callback(result, null);
                    };

                TreggUser.SignUp("foo", "foobar", callbackSignUp);
            }
        }

        [TestMethod]// This test must FAIL unless change callback func
        public void TestSignUpEx()
        {
            using (ShimsContext.Create())
            {
                ShimTreggAPI.CreateHttpRequestStringStringDictionaryOfStringStringTreggAPITreggApiDelegateBooleanString = (string url, string method, Dictionary<string, string> data, TreggAPI.TreggApiDelegate callback, bool isAuthenticated, string contentType) =>
                {
                    int statusCode = 400;
                    TreggException ex = new TreggException("Request failed: status code " + statusCode.ToString());

                    callback(null, ex);
                };

                TreggUser.SignUp("foo", "foobar", callbackSignUp);
            }
        }

        [TestMethod]
        public void TestSignIn()
        {
            using (ShimsContext.Create())
            {
                ShimTreggAPI.CreateHttpRequestStringStringDictionaryOfStringStringTreggAPITreggApiDelegateBooleanString = (string url, string method, Dictionary<string, string> data, TreggAPI.TreggApiDelegate callback, bool isAuthenticated, string contentType) =>
                {
                    string responseString = "{\"user\":{\"id\":10,\"created_at\":\"2013-10-04T13:43:31.055Z\",\"updated_at\":\"2013-10-04T13:43:31.055Z\",\"username\":\"foo\",\"password\":\"$2a$10$xJKoqUeA7IAwebdNHNkeie/x.gmjvQZEO83cTy.44pSyBpVF9lxWq\",\"reset_pwd_token\":null,\"password_reset_sent_at\":null,\"facebook_uid\":null,\"facebook_first_name\":null,\"facebook_last_name\":null,\"facebook_username\":null,\"facebook_picture\":null,\"facebook_gender\":null,\"facebook_birthday\":null,\"role\":null},\"access_token\":\"iqcpdpxexqbstiw766l8fz29l9k3fsd\"}";
                    var result = MiniJSON.Json.Deserialize(responseString) as Dictionary<string, object>;
                    responseAccessToken = (string)result["access_token"];

                    callback(result, null);
                };

                TreggUser.SignIn("foo", "foobar", callbackSignIn);
            }
        }

        [TestMethod]// This test must FAIL unless change callback func
        public void TestSignInEx()
        {
            using (ShimsContext.Create())
            {
                ShimTreggAPI.CreateHttpRequestStringStringDictionaryOfStringStringTreggAPITreggApiDelegateBooleanString = (string url, string method, Dictionary<string, string> data, TreggAPI.TreggApiDelegate callback, bool isAuthenticated, string contentType) =>
                {
                    int statusCode = 400;
                    TreggException ex = new TreggException("Request failed: status code " + statusCode.ToString());

                    callback(null, ex);
                };

                TreggUser.SignIn("foo", "foobar", callbackSignIn);
            }
        }

        [TestMethod]
        public void TestSignInWithFacebook()
        {
            using (ShimsContext.Create())
            {
                ShimTreggAPI.CreateHttpRequestStringStringDictionaryOfStringStringTreggAPITreggApiDelegateBooleanString = (string url, string method, Dictionary<string, string> data, TreggAPI.TreggApiDelegate callback, bool isAuthenticated, string contentType) =>
                {
                    string responseString = "{\"user\":{\"id\":10,\"created_at\":\"2013-10-04T13:43:31.055Z\",\"updated_at\":\"2013-10-04T13:43:31.055Z\",\"username\":\"foo\",\"password\":\"$2a$10$xJKoqUeA7IAwebdNHNkeie/x.gmjvQZEO83cTy.44pSyBpVF9lxWq\",\"reset_pwd_token\":null,\"password_reset_sent_at\":null,\"facebook_uid\":null,\"facebook_first_name\":null,\"facebook_last_name\":null,\"facebook_username\":null,\"facebook_picture\":null,\"facebook_gender\":null,\"facebook_birthday\":null,\"role\":null},\"access_token\":\"iqcpdpxexqbstiw766l8fz29l9k3fsd\"}";
                    var result = MiniJSON.Json.Deserialize(responseString) as Dictionary<string, object>;
                    responseAccessToken = (string)result["access_token"];

                    callback(result, null);
                };

                TreggUser.SignInWithFacebook("iqcpdpxexqbstiw766l8fz29l9k3fsd", callbackSignInWithFB);
            }
        }

        [TestMethod]// This test must FAIL unless change callback func
        public void TestSignInWithFacebookEx()
        {
            using (ShimsContext.Create())
            {
                ShimTreggAPI.CreateHttpRequestStringStringDictionaryOfStringStringTreggAPITreggApiDelegateBooleanString = (string url, string method, Dictionary<string, string> data, TreggAPI.TreggApiDelegate callback, bool isAuthenticated, string contentType) =>
                {
                    int statusCode = 400;
                    TreggException ex = new TreggException("Request failed: status code " + statusCode.ToString());

                    callback(null, ex);
                };

                TreggUser.SignInWithFacebook("iqcpdpxexqbstiw766l8fz29l9k3fsd", callbackSignInWithFB);
            }
        }

        [TestMethod]
        public void TestCurrentUser()
        {
            using (ShimsContext.Create())
            {
                ShimTreggAPI.CreateHttpRequestStringStringDictionaryOfStringStringTreggAPITreggApiDelegateBooleanString = (string url, string method, Dictionary<string, string> data, TreggAPI.TreggApiDelegate callback, bool isAuthenticated, string contentType) =>
                {
                    string responseString = "{\"user\":{\"id\":10,\"created_at\":\"2013-10-04T13:43:31.055Z\",\"updated_at\":\"2013-10-04T13:43:31.055Z\",\"username\":\"foo\",\"password\":\"$2a$10$xJKoqUeA7IAwebdNHNkeie/x.gmjvQZEO83cTy.44pSyBpVF9lxWq\",\"reset_pwd_token\":null,\"password_reset_sent_at\":null,\"facebook_uid\":null,\"facebook_first_name\":null,\"facebook_last_name\":null,\"facebook_username\":null,\"facebook_picture\":null,\"facebook_gender\":null,\"facebook_birthday\":null,\"role\":null},\"access_token\":\"iqcpdpxexqbstiw766l8fz29l9k3fsd\"}";
                    var result = MiniJSON.Json.Deserialize(responseString) as Dictionary<string, object>;
                    responseAccessToken = (string)result["access_token"];

                    callback(result, null);
                };

                TreggUser.SignUp("foo", "foobar", callbackSignIn);

                TreggUser.CurrentUser();
            }
        }

        [TestMethod]// This test must FAIL unless change callback func
        public void TestCurrentUserEx()
        {
            using (ShimsContext.Create())
            {
                ShimTreggAPI.CreateHttpRequestStringStringDictionaryOfStringStringTreggAPITreggApiDelegateBooleanString = (string url, string method, Dictionary<string, string> data, TreggAPI.TreggApiDelegate callback, bool isAuthenticated, string contentType) =>
                {
                    int statusCode = 400;
                    TreggException ex = new TreggException("Request failed: status code " + statusCode.ToString());

                    callback(null, ex);
                };

                TreggUser.SignUp("foo", "foobar", callbackSignIn);

                TreggUser.CurrentUser();
            }
        }
    }
}
