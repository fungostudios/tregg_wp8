﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Tregg;

namespace TreggAppSample
{
    public partial class SignUpPage : PhoneApplicationPage
    {

        public SignUpPage()
        {
            InitializeComponent();
        }

        void callbackSignUp(TreggUser user, TreggException e)
        {

        }

        private void ConfirmSignUp_Click(object sender, RoutedEventArgs e)
        {
            if (UsernameBox.Text != null && PasswordBox.Password != null)
            {
                TreggUser.SignUp(UsernameBox.Text, PasswordBox.Password, callbackSignUp);
            }
        }     
    }
}