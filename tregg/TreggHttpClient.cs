﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Tregg
{
    public class TreggHttpClient
    {
        public static string responseStatusCode = "";

        public delegate void TreggHttpDelegate(String responseStatus, String responseJsonHeaders, String responseBody, TreggException e);

        public static void CreatePostRequest(String url, String jsonParams, String jsonHeaders, TreggHttpDelegate callback)
        {
            CreateHttpRequest(url, "POST", jsonParams, jsonHeaders, callback);
        }

        public static void CreateGetRequest(String url, String jsonParams, String jsonHeaders, TreggHttpDelegate callback)
        {
            CreateHttpRequest(url, "GET", jsonParams, jsonHeaders, callback);
        }

        public static void CreatePutRequest(String url, String jsonParams, String jsonHeaders, TreggHttpDelegate callback)
        {
            CreateHttpRequest(url, "PUT", jsonParams, jsonHeaders, callback);
        }

        public static void CreateDeleteRequest(String url, String jsonParams, String jsonHeaders, TreggHttpDelegate callback)
        {
            CreateHttpRequest(url, "DELETE", jsonParams, jsonHeaders, callback);
        }

        public async static void CreateHttpRequest(string url, string method, String jsonParams, String jsonHeaders, TreggHttpDelegate callback)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(TreggConfig.baseUrl + url);
            request.Method = method;

            var headerString = MiniJSON.Json.Deserialize(jsonHeaders) as Dictionary<string, object>;
            foreach (KeyValuePair<string, object> pair in headerString)
            {
                request.Headers[pair.Key] = pair.Value.ToString();
            }

            Stream postStream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null);
            byte[] byteArray = Encoding.UTF8.GetBytes(jsonParams);
            postStream.Write(byteArray, 0, jsonParams.Length);

            postStream.Close();
            request.BeginGetResponse((IAsyncResult iar) =>
            {
                GetResponseCallback(iar, callback);
            },
            request);
        }

        private static void GetResponseCallback(IAsyncResult asynchronousResult, TreggHttpDelegate callback)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                string responseBodyString = streamRead.ReadToEnd();
                responseStatusCode = (response.StatusCode).ToString();

                System.Diagnostics.Debug.WriteLine(responseStatusCode);
                System.Diagnostics.Debug.WriteLine(responseBodyString);

                var responseHeaders = response.Headers;
                string[] headerKeys = responseHeaders.AllKeys;
                string[] headerValues = new string[responseHeaders.Count];

                Dictionary<string, string> headerDict = new Dictionary<string, string>();

                for (int i = 0; i < responseHeaders.Count; i++)
                {
                    headerDict.Add(headerKeys[i], headerValues[i]);
                }

                string responseHeaderString = MiniJSON.Json.Serialize(headerDict);

                TreggException ex = null;
                if (responseStatusCode != "200" && responseStatusCode != "201")
                {
                    ex = new TreggException("Request failed: status code " + responseStatusCode);
                }

                streamResponse.Close();
                streamRead.Close();
                response.Close();

                callback(responseStatusCode, responseHeaderString, responseBodyString, ex);
            }
            catch (WebException ex)
            {
                Stream streamResponse = ex.Response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                string responseString = streamRead.ReadToEnd();

                System.Diagnostics.Debug.WriteLine(responseString);
                TreggException exc = new TreggException(ex.Message);

                streamResponse.Close();
                streamRead.Close();
                ex.Response.Close();

                callback(responseStatusCode, null, null, exc);
            }
        }
    }
}
