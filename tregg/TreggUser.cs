using System;
using System.Windows;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.IO.IsolatedStorage;
using System.Text;

namespace Tregg
{
    public enum providerType { Facebook };

    public class TreggUser
    {
        public delegate void TreggUserDelegate(TreggUser user, TreggException e);
        public delegate void TreggExceptionDelegate(TreggException e);

        private string userName;
        private string password;
        private string facebookUid;
        private int userId;
        private static TreggUser currentUser = null;

        public string UserName
        {
            set { userName = value; }
            get { return userName; }
        }

        public string Password
        {
            set { password = value; }
            get { return password; }
        }

        public string FacebookUid
        {
            set { facebookUid = value; }
            get { return facebookUid; }
        }

        public int Uid
        {
            set { userId = value; }
            get { return userId; }
        }

        public TreggUser() { }

        public TreggUser(string name, string pwd)
        {
            this.UserName = name;
            this.Password = pwd;
        }

        public static void SignUp(string username, string pwd, TreggUserDelegate callback)
        {
            TreggUser user = new TreggUser(username, pwd);
            user.SignUp(callback);
        }

        public void SignUp(TreggUserDelegate callback)
        {
            Dictionary<string, string> dataToPost = new Dictionary<string, string>();
            dataToPost.Add("client_id", TreggConfig.clientId);
            dataToPost.Add("username", this.UserName);
            dataToPost.Add("password", this.Password);

            TreggAPI.CreatePostRequest("/sign-up", dataToPost, (Dictionary<string, object> responseData, TreggException e) =>
            {
                SignUpResponseCallback(responseData, e, callback);
            }, false);            
        }

        private void SignUpResponseCallback(Dictionary<string, object> responseData, TreggException e, TreggUserDelegate callback)
        {
            if (e == null)
            {
                string accessToken = (string)responseData["access_token"];
                IsolatedStorageSettings userPrivateData = IsolatedStorageSettings.ApplicationSettings;
                userPrivateData.Add("access_token", accessToken);
                userPrivateData.Save();

                Dictionary<string, object> userData = responseData["user"] as Dictionary<string, object>;
                this.Uid = Convert.ToInt32(userData["id"]);
                currentUser = this;
                callback(this, e);
            } 
            else
            {
                TreggException ex = e;
                try
                {
                    ex = new TreggException ((string)responseData["error"]);
                }
                catch (System.Exception) { }

                callback(null, ex);
            }
        }

        public static void SignIn(string username, string password, TreggUserDelegate callback)
        {
            Dictionary<string, string> dataToPost = new Dictionary<string, string>();
            dataToPost.Add("grant_type", "password");
            dataToPost.Add("client_id", TreggConfig.clientId);
            dataToPost.Add("username", username);
            dataToPost.Add("password", password);

            TreggAPI.CreatePostRequest("/oauth/authorize", dataToPost, (Dictionary<string, object> responseData, TreggException e) => 
            { 
                SigninResponseCallback(responseData, e, callback);
            }, false);
        }

        private static void SigninResponseCallback(Dictionary<string, object> responseData, TreggException e, TreggUserDelegate callback)
        {
            if (e == null)
            {
                string accessToken = (string)responseData["access_token"];
                IsolatedStorageSettings userPrivateData = IsolatedStorageSettings.ApplicationSettings;
                userPrivateData.Add("access_token", accessToken);
                userPrivateData.Save();

                TreggAPI.CreateGetRequest("/me", new Dictionary<string, string>(), (Dictionary<string, object> response, TreggException ex) => 
                { 
                    GetMeResponseCallback(response, ex, callback);
                }, true);
            }
            else
            {
                TreggException ex = e;
                try
                {
                    ex = new TreggException((string)responseData["error"]);
                }
                catch (System.Exception) { }

                callback(null, ex);
            }
        }

        private static void GetMeResponseCallback(Dictionary<string, object> responseData, TreggException e, TreggUserDelegate callback) 
        {
            if (e == null)
            {
                Dictionary<string, object> userData = responseData["user"] as Dictionary<string, object>;
                string username = (string)userData["username"];
                string fbUid = (string)userData["facebook_uid"];
                int id = Convert.ToInt32(userData["id"]);
                TreggUser user = new TreggUser();
                user.UserName = username;
                user.Uid = id;
                user.FacebookUid = fbUid;
                callback(user, e);
            }
            else
            {
                TreggException ex = e;
                try
                {
                    ex = new TreggException((string)responseData["error"]);
                }
                catch (System.Exception) { }
            }
        }

        private static void SignIn(providerType provider, string accessToken, TreggUserDelegate callback)
        {
            if (provider == providerType.Facebook)
            {
                SignInWithFacebook(accessToken, callback);
            }
            else
            {
                TreggException ex = new TreggException("error: user doesn't exist");
            }
        }

        public static void SignInWithFacebook(string accessToken, TreggUserDelegate callback)
        {
            Dictionary<string, string> dataToPost = new Dictionary<string, string>();
            dataToPost.Add("grant_type", "assertion");
            dataToPost.Add("client_id", TreggConfig.clientId);
            dataToPost.Add("assertion_type", "https://graph.facebook.com");
            dataToPost.Add("assertion", accessToken);

            TreggAPI.CreatePostRequest("/oauth/authorize", dataToPost, (Dictionary<string, object> responseData, TreggException e) =>
            {
                SigninWithFacebookResponseCallback(responseData, e, callback);
            }, false);
        }

        private static void SigninWithFacebookResponseCallback(Dictionary<string, object> responseData, TreggException e, TreggUserDelegate callback)
        {
            if (e == null)
            {
                string accessToken = (string)responseData["access_token"];
                IsolatedStorageSettings userPrivateData = IsolatedStorageSettings.ApplicationSettings;
                userPrivateData.Add("access_token", accessToken);
                userPrivateData.Save();

                TreggAPI.CreateGetRequest("/me", new Dictionary<string, string>(), (Dictionary<string, object> response, TreggException ex) =>
                {
                    GetMeResponseCallback(response, ex, callback);
                }, false);
            }
            else
            {
                TreggException ex = e;
                try
                {
                    ex = new TreggException((string)responseData["error"]);
                }
                catch (System.Exception) { }

                callback(null, ex);
            }
        }

        public void SignOut()
        {
                TreggAPI.CreatePostRequest("/sign-out",new Dictionary<string, string>(), SignOutResponseCallback, true);
        }

        private static void SignOutResponseCallback(Dictionary<string, object> responseData, TreggException e)
        {
            if (e == null)
            {
                string result = (string)responseData["success"];
            }
            else
            {
                TreggException ex = e;
                try
                {
                    ex = new TreggException((string)responseData["error"]);
                }
                catch (System.Exception) { }
            }
        }

        public void LinkFacebook(string accessToken, TreggUserDelegate callback)
        {
            Dictionary<string, string> dataToPost = new Dictionary<string, string>();
            dataToPost.Add("facebook_token", accessToken);

            TreggAPI.CreatePostRequest("/me/facebook_connect", dataToPost, (Dictionary<string, object> responseData, TreggException e) =>
            {
                LinkFacebookResponseCallback(responseData, e, callback);
            }, true);
        }

        private void LinkFacebookResponseCallback(Dictionary<string, object> responseData, TreggException e, TreggUserDelegate callback)
        {
            if (e == null)
            {
                string username = (string)responseData["user"];
                this.UserName = username;
                callback(this, e);
            }
            else
            {
                TreggException ex = e;
                try
                {
                    ex = new TreggException((string)responseData["error"]);
                }
                catch (System.Exception) { }

                callback(null, ex);
            }
        }

        public static TreggUser CurrentUser()
        {
            if (currentUser != null)
            {
                return currentUser;
            } 
            else
            {
                TreggException ex = new TreggException("User doesn't exists.");
                return null;
            } 
        }

    }
}