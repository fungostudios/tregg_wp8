﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Tregg
{
    public class TreggAPI
    {
        public delegate void TreggApiDelegate(Dictionary<string, object> responseData, TreggException e);

        public static void CreatePostRequest(string url, Dictionary<string, string> data, TreggApiDelegate callback,  bool isAuthenticated, string contentType = "application/x-www-form-urlencoded")
        {
            CreateHttpRequest(url, "POST", data, callback, isAuthenticated, contentType);
        }

        public static void CreateGetRequest(string url, Dictionary<string, string> data, TreggApiDelegate callback, bool isAuthenticated)
        {
            CreateHttpRequest(url, "GET", data, callback, isAuthenticated);
        }

        public async static void CreateHttpRequest(string url, string method, Dictionary<string, string> data, TreggApiDelegate callback, bool isAuthenticated, string contentType = "application/json")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(TreggConfig.baseUrl + url);
            request.ContentType = contentType;
            request.Method = method;

            if (isAuthenticated)
            {
                request.Headers[HttpRequestHeader.Authorization] = "OAuth " + data["access_token"];
            }

            Stream postStream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null);
            string postData = string.Join("&", data.Select(d => d.Key + "=" + d.Value).ToArray());
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            postStream.Write(byteArray, 0, postData.Length);

            postStream.Close();
            request.BeginGetResponse((IAsyncResult iar) => 
            {
                GetResponseCallback(iar, callback);
            }, 
            request);
        }
   
        private static void GetResponseCallback(IAsyncResult asynchronousResult, TreggApiDelegate callback)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                string responseString = streamRead.ReadToEnd();
                int statusCode = Convert.ToInt32(response.StatusCode);

                System.Diagnostics.Debug.WriteLine(statusCode);
                System.Diagnostics.Debug.WriteLine(responseString);

                var result = MiniJSON.Json.Deserialize(responseString) as Dictionary<string, object>;

                TreggException ex = null;
                if (statusCode != 200 && statusCode != 201)
                {
                    ex = new TreggException("Request failed: status code " + statusCode.ToString());
                }

                streamResponse.Close();
                streamRead.Close();
                response.Close();

                callback(result, ex);
            }
            catch (WebException ex)
            {
                Stream streamResponse = ex.Response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                string responseString = streamRead.ReadToEnd();

                System.Diagnostics.Debug.WriteLine(responseString);
                TreggException exc = new TreggException(ex.Message);

                streamResponse.Close();
                streamRead.Close();
                ex.Response.Close();

                callback(null, exc);
            }
        }
    }
}
