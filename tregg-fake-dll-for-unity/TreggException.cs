﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Tregg
{
    public class TreggException : Exception
    {
        public TreggException(string message) : base(message) { }
    }
}
