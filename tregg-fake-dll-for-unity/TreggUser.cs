﻿using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.IO.IsolatedStorage;
using System.Text;

namespace Tregg
{
    public enum providerType { Facebook };

    public class TreggUser
    {
        public delegate void TreggUserDelegate(TreggUser user, TreggException e);
        public delegate void TreggExceptionDelegate(TreggException e);

        private string userName;
        private string password;
        private string facebookUid;
        private int userId;
        private static TreggUser currentUser = null;

        public string UserName
        {
            set { userName = value; }
            get { return userName; }
        }

        public string Password
        {
            set { password = value; }
            get { return password; }
        }

        public string FacebookUid
        {
            set { facebookUid = value; }
            get { return facebookUid; }
        }

        public int Uid
        {
            set { userId = value; }
            get { return userId; }
        }

        public TreggUser() { }

        public TreggUser(string name, string pwd)
        {
            this.UserName = name;
            this.Password = pwd;
        }

        public static void SignUp(string username, string pwd, TreggUserDelegate callback)
        {

        }

        public void SignUp(TreggUserDelegate callback)
        {

        }

        private void SignUpResponseCallback(Dictionary<string, object> responseData, TreggException e, TreggUserDelegate callback)
        {
            
        }

        public static void SignIn(string username, string password, TreggUserDelegate callback)
        {
           
        }

        private static void SigninResponseCallback(Dictionary<string, object> responseData, TreggException e, TreggUserDelegate callback)
        {
           
        }

        private static void GetMeResponseCallback(Dictionary<string, object> responseData, TreggException e, TreggUserDelegate callback)
        {
            
        }

        private static void SignIn(providerType provider, string accessToken, TreggUserDelegate callback)
        {
            
        }

        public static void SignInWithFacebook(string accessToken, TreggUserDelegate callback)
        {
            
        }

        private static void SigninWithFacebookResponseCallback(Dictionary<string, object> responseData, TreggException e, TreggUserDelegate callback)
        {
            
        }

        public void SignOut()
        {
        }

        private static void SignOutResponseCallback(Dictionary<string, object> responseData, TreggException e)
        {
            
        }

        public void LinkFacebook(string accessToken, TreggUserDelegate callback)
        {
            
        }

        private void LinkFacebookResponseCallback(Dictionary<string, object> responseData, TreggException e, TreggUserDelegate callback)
        {
            
        }

        public static TreggUser CurrentUser()
        {
            if (currentUser != null)
            {
                return currentUser;
            }
            else
            {
                TreggException ex = new TreggException("User doesn't exists.");
                return null;
            }
        }

    }
}