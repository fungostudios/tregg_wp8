﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Tregg
{
    public class TreggAPI
    {
        public delegate void TreggApiDelegate(Dictionary<string, object> responseData, TreggException e);

        public static void CreatePostRequest(string url, Dictionary<string, string> data, TreggApiDelegate callback, bool isAuthenticated, string contentType = "application/x-www-form-urlencoded")
        {
            
        }

        public static void CreateGetRequest(string url, Dictionary<string, string> data, TreggApiDelegate callback, bool isAuthenticated)
        {
           
        }

        public static void CreateHttpRequest(string url, string method, Dictionary<string, string> data, TreggApiDelegate callback, bool isAuthenticated, string contentType = "application/json")
        {

        }

        private static void GetResponseCallback(IAsyncResult asynchronousResult, TreggApiDelegate callback)
        {
            
            
        }
    }
}
